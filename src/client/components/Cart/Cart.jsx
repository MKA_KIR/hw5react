import React from "react";
import CartItem from '../CartItem'
import './Cart.css'
import {useDispatch, useSelector} from "react-redux";
import {decreaseFromCartAction, increaseFromCartAction,delFromCartAction} from "../../../store/actions";
const Cart = () =>{
    const cart = useSelector(state => state.cart);
    const dispatch = useDispatch();

    const cartElements = cart.map((item, index) => <CartItem
        decreaseItem={()=>dispatch(decreaseFromCartAction(index))}
        increaseItem={()=>dispatch(increaseFromCartAction(index))}
        deleteItem={()=>dispatch(delFromCartAction(index))}
        {...item} />)
    let totalPrice = 0;
    cart.forEach(({count, price})=>totalPrice+=count*price)
    return(
        <>
            <table className="show-cart table">
                {cartElements}
            </table>
            <div>Total price: ${totalPrice}</div>
         </>
    )
}
export default Cart

import React from 'react';
import Navbar from '../src/client/Navbar'
import ProductsListPage from '../src/client/Products/pages/ProductsListPage'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Cart from "./client/components/Cart";
import Modal from "./Shared/Components/Modal";
import store from "./store/store";
import {Provider} from 'react-redux';


function App() {
  return (
      <Provider store={store}>
    <div className="App">
        <Navbar />
      <ProductsListPage />
        <Modal title='Cart'>
            <Cart/>
        </Modal>
    </div>
      </Provider>
  );
}

export default App;
